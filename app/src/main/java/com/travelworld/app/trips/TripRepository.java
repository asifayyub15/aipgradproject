package com.travelworld.app.trips;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;


public interface TripRepository extends JpaRepository<Trip, String> {
	
	Optional<Trip> findByEmail(String email); 
}
